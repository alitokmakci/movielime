# MovieLime
* Designed with **bulma** and **vue cli**<br>
* Used scss stylesheets to be more understandable and fast programming.<br>
* For good looking and readability I used **ESLint** and **Prettier**.
